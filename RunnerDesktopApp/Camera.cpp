#include <iostream>
#include <sstream> // for ostringstream
#include <string>
#include "Camera.h"

Camera::Camera()
{
}

Camera::Camera(Rect* bounds)
{
	this->viewport = bounds;
}


Camera::~Camera()
{
}

float Camera::getX()
{
	return viewport->x;
}

float Camera::getY()
{
	return viewport->y;
}

void Camera::Update()
{
	this->viewport->x = target->getX() - (0.3f * this->viewport->width);
	//this->viewport = new Rect(target->getX() - (this->viewport->width / 2.f), this->viewport->y, this->viewport->width, this->viewport->height);

	//std::ostringstream debugViewportX;
	//debugViewportX << "Camera::Update - viewport->x " << this->viewport->x << "\n";
	//OutputDebugStringA(debugViewportX.str().c_str());
}

Vector2 Camera::WorldToScreenPoint(float pX, float pY)
{
	// screen 1024x768
	// viewport 20x10
	// ratio = (1024 - 0) / (20 - 0)
	// y = (x - viewport_x1) * ratio + screen_x1

	//float screenX = this->x * Game

	float worldX = pX - getX();
	float worldY = pY - getY();
	float screenX = worldX * GameConstants::PIXELS_PER_WORLD_UNIT;
	float screenY = GameConstants::SCREEN_HEIGHT - (worldY * GameConstants::PIXELS_PER_WORLD_UNIT);
	return Vector2(screenX, screenY);
}

void Camera::SetTarget(PlayerHelper * player)
{
	this->target = player;
}
