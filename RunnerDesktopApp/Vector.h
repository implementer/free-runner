#pragma once

struct Vector2
{
public:
	float x, y;

	Vector2(float pX, float pY) {
		this->x = pX;
		this->y = pY;
	}
};
