#include <iostream>
#include <sstream> // for ostringstream
#include <string>
#include "MapHelper.h"


MapHelper::MapHelper(HGE* pHge)
{
	this->hge = pHge;
	groundTexture = hge->Texture_Load("ground.png");
	groundTexture2 = hge->Texture_Load("ground2.png");

	if (!groundTexture || !groundTexture2)
	{
		// If one of the data files is not found, display
		// an error message and shutdown.
		MessageBox(NULL, _T("Can't load one of the following files:\nbrick.png"), _T("Error"), MB_OK | MB_ICONERROR | MB_APPLMODAL);
		hge->System_Shutdown();
		hge->Release();
		return;
	}

	groundSprite = new hgeSprite(groundTexture, 0, 0, 128, 128);
	groundSprite2 = new hgeSprite(groundTexture2, 0, 0, 128, 128);

	for (int index = 0; index < GameConstants::VIEWPORT_WIDTH; index++) {
		ground[index] = new hgeSprite(groundTexture, 0, 0, TILE_GROUND_SIZE, TILE_GROUND_SIZE);
	}

}
MapHelper::~MapHelper()
{
	delete groundSprite;
	delete groundSprite2;
	hge->Texture_Free(groundTexture);
	hge->Texture_Free(groundTexture2);
};



void MapHelper::Update()
{

}

void MapHelper::Render(Camera* pCamera) {
	//OutputDebugString(_T("MapHelper::Render()\n"));

	for (int index = 0; index < GameConstants::VIEWPORT_WIDTH; index++) {
		Vector2 position = pCamera->WorldToScreenPoint(index, 1.f);
		ground[index]->RenderEx(position.x, position.y, 0.f, GameConstants::PIXELS_PER_WORLD_UNIT / TILE_GROUND_SIZE, GameConstants::PIXELS_PER_WORLD_UNIT / TILE_GROUND_SIZE);

		//std::ostringstream debugPos;
		//debugPos << "MapHelper::Render - index " << index << " position x " << position.x << "\n";
		//OutputDebugStringA(debugPos.str().c_str());
	}
};