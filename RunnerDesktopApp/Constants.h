#pragma once

namespace GameConstants {

	const int SCREEN_WIDTH = 1024;
	const int SCREEN_HEIGHT = 768;

	const int VIEWPORT_WIDTH = 20;
	const int VIEWPORT_HEIGHT = 10;

	const float PIXELS_PER_WORLD_UNIT = SCREEN_WIDTH / VIEWPORT_WIDTH;
	const float SCREEN_TO_WORLD_RATIO_Y = SCREEN_HEIGHT / VIEWPORT_HEIGHT;
}