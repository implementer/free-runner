#pragma once

#include "stdafx.h"
#include "hge.h"
#include "hgesprite.h"
#include "Constants.h"
#include "Camera.h"

class MapHelper
{
private:

	const float TILE_GROUND_SIZE = 128.f;

	HGE* hge;
	HTEXTURE groundTexture;
	HTEXTURE groundTexture2;

	hgeSprite* groundSprite;
	hgeSprite* groundSprite2;
	hgeSprite* ground [GameConstants::VIEWPORT_WIDTH];
	
public:
	MapHelper(HGE* pHge);
	~MapHelper();
	
	void Update();
	void Render(Camera* pCamera);
};