#pragma once

#include "stdafx.h"
#include "hge.h"
#include "hgeanim.h"
#include "Camera.h"

class Camera;

class PlayerHelper
{
public:
	PlayerHelper(HGE* pHge, Camera* pCamera);
	~PlayerHelper();

	void Update();
	void Render();

	float getX();
	float getY();

private:

	const float PLAYER_WIDTH = 108.f;
	const float PLAYER_HEIGHT = 140.f;
	
	const float PLAYER_SPEED = 6.f;

	const float GRAVITY = -9.f;

	float x, y;
	float dx = 0.0f, dy = 0.0f;
	float yVelocity = 0.0f, yAccel = 0.0f;

	bool isJumpKeyPressed = false;
	bool isJumping = false;

	HGE * hge;
	HTEXTURE texture;
	hgeAnimation* animation;
	Camera* camera;

	void LoadResources();
};

