#pragma once

#include "Rect.h"
#include "Vector.h"
#include "Constants.h"
#include "PlayerHelper.h"

class PlayerHelper;

class Camera
{
public:
	Camera();
	Camera(Rect * bounds);
	~Camera();

	float getX();
	float getY();

	void Update();
	
	Vector2 WorldToScreenPoint(float x, float y);
	void SetTarget(PlayerHelper* player);

private:
	Rect* viewport;
	PlayerHelper* target;
};