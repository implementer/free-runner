#include <iostream>
#include <sstream> // for ostringstream
#include <string>
#include "PlayerHelper.h"


const float GROUND_LEVEL = 2.f;

PlayerHelper::PlayerHelper(HGE* pHge, Camera* pCamera)
{
	this->hge = pHge;
	this->LoadResources();

	this->x = 0;
	this->y = GROUND_LEVEL;
	this->camera = pCamera;

	animation->Play();
}

PlayerHelper::~PlayerHelper()
{
	delete animation;
	hge->Texture_Free(texture);
}

void PlayerHelper::Update()
{
	const float deltaTime = hge->Timer_GetDelta();

	this->x = this->x + PLAYER_SPEED * deltaTime;

	isJumpKeyPressed = hge->Input_GetKeyState(HGEK_SPACE);

	bool isTouchingGround = this->y < GROUND_LEVEL + 0.1f;
	// Allow player to jump
	if (isTouchingGround) {
		yVelocity = 0.0f;
		isJumping = false;
	}

	if (isJumpKeyPressed && !isJumping) {
		yVelocity = 10.0f;
		isJumping = true;
	}

	if (!isJumpKeyPressed && isJumping) {
		if (yVelocity > 2.5f) {
			yVelocity = 2.5f;
		}
	}

	if (isJumping) {
		this->y += yVelocity * deltaTime;
		yVelocity += GRAVITY * deltaTime;
	}

	std::ostringstream debugPlayer;
	debugPlayer << "PlayerHelper::Update - y " << this->y << " isJumpKeyPressed " << isJumpKeyPressed <<"\n";
	OutputDebugStringA(debugPlayer.str().c_str());

	animation->Update(deltaTime);
}

void PlayerHelper::Render()
{
	//OutputDebugString(_T("PlayerHelper::Render()\n"));
	Vector2 position = camera->WorldToScreenPoint(this->x, this->y);

	//std::ostringstream debugPos;
	//debugPos << "PlayerHelper::Render - position x " << position.x << "\n";
	//OutputDebugStringA(debugPos.str().c_str());

	animation->RenderEx(position.x, position.y, 0.f, GameConstants::PIXELS_PER_WORLD_UNIT / PLAYER_WIDTH, GameConstants::PIXELS_PER_WORLD_UNIT / PLAYER_HEIGHT);
}

float PlayerHelper::getX()
{
	return this->x;
}

float PlayerHelper::getY()
{
	return this->y;
}

void PlayerHelper::LoadResources()
{
	texture = hge->Texture_Load("character.png");
	animation = new hgeAnimation(texture, 8, 6.f, 0.f, 0.f, PLAYER_WIDTH, PLAYER_HEIGHT);
}
